# Parcel.js

La documentacion que se presenta en "https://es.parceljs.org/getting_started.html" indica como debes utilizar para el inicio asi como la ejecución

utilizando los comandos para ejecución local y de producción

# Para ejecutarlo en modo de desarrollo 
yarn dev
# o
npm run dev

# Para ejecutarlo en modo de producción
yarn build
# o
npm run build.

# Instalación 

Aqui tendremos una guia mucho mas clara de como instalar Parcel y ademas una ejemplificación para ir trabajando de manera local

https://bluuweb.github.io/webpack/parcel-js/#iniciar-npm