import GenericForm from './GenericForm'

let formularios = document.querySelectorAll('form[data-module=generic-form]')

for (let i = 0; i < formularios.length; i++) {
  const formularioEl = formularios[i];
  new GenericForm(formularioEl)
}