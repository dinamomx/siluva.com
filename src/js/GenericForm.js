function parseJSONFromDataAndDefault(dataString, defaultValue) {
  try {
    return JSON.parse(dataString) || defaultValue;
  } catch (error) {
    return defaultValue;
  }
}

export default class GenericForm {
  /**
   * Port del mixin generico de vue de envio de formularios
   *
   * @param {HTMLFormElement} form El formulario
   */
  constructor(form) {
    this.$form = form;
    /**
     * Url de envío
     *
     * @type {string}
     */
    this.endpoint = form.dataset.endpoint || form.getAttribute("action");
    if (!this.endpoint) {
      throw new Error(
        "Falta endpoint en [data-endpoint] o [action]",
        this.$form
      );
    }
    // this.endpoint = new URL(this.endpoint).toString();
    /**
     * Título del mail
     */
    this.mailTitle = form.dataset.mailTitle || "Formulario en siluva.com";
    /**
     * Hoja del drive a usar
     */
    this.sheetDestiny = form.dataset.sheetDestiny || "Contacto";
    /**
     * Orden en el que se muestran los datos en el correo,
     * lo que no esté incluido aquí es omitido.
     *
     * @type {string[]}
     */
    this.formDataOrder = parseJSONFromDataAndDefault(
      form.dataset.formDataOrder,
      ["Fecha", "NombreCompleto", "Telefono", "CorreoElectronico", "Mensaje"]
    );
    /**
     * Correo al que se envía este lead
     */
    this.sendTo = form.dataset.sendTo || "ramses@wdinamo.com";
    /**
     * Correo al que las respuestas son enviadas
     */
    this.replyTo = "";
    /**
     * Asunto del correo
     */
    this.subject =
      form.dataset.subject || "Nuevo mensaje desde siluva.com";
    /**
     * Nombre del remitente
     */
    this.senderName = "Página web siluva.com";
    /**
     * Con copia
     */
    this.copyTo = "";
    /**
     * Copia oculta
     */
    this.ocultCopyTo = "";
    /**
     * Correo al que se envía las notificaciones de fallos.
     */
    this.debugMail = "ramses@wdinamo.com";
    /** Thank you page */
    this.thankyou = this.$form.dataset.thanks;

    /** Estados */
    // Estado de carga
    this.isLoading = false;
    // Bloqueo del botón y evento de envios
    this.preventSending = false;
    // Envío exitoso
    this.hasSucceded = false;
    // Envío fallo
    this.hasFailed = false;
    /** @type {[name: string]: string|number} */
    this.model = {
      Fecha: "",
      Nombre: "",
      Teléfono: "",
      Correo: "",
      Mensaje: "",
    };

    // Datos iniciales del modelo
    const today = new Date();
    this.model.Fecha = today.toLocaleString("es");
    // Añadiendo evento al elemento del formulario
    this.$form.addEventListener("submit", this.send.bind(this));

    // Elementos para mostrar feedback
    this.$loading = this.$form.querySelector("[data-loading]");
    this.$error = this.$form.querySelector("[data-error]");
    this.$success = this.$form.querySelector("[data-success]");
    this.activeClass = "is-active";

    this.inputs = this.$form.querySelectorAll("[data-model]");
    for (let i = 0; i < this.inputs.length; i++) {
      const input = this.inputs[i];
      const modelKey = input.dataset.model || input.getAttribute("name");
      if (!modelKey) {
        // Abort if no key is specified
        // eslint-disable-next-line no-continue
        continue;
      }
      const eventCallback = () => {
        this.model[modelKey] = input.value;
      };
      input.addEventListener("input", eventCallback);
      input.addEventListener("change", eventCallback);
    }
  }

  get payload() {
    return {
      sheetDestiny: this.sheetDestiny,
      formDataOrder: this.formDataOrder,
      sendTo: this.sendTo,
      replyTo: this.replyTo,
      subject: this.subject,
      debugMail: this.debugMail,
      senderName: this.senderName,
      ocultCopyTo: this.ocultCopyTo,
      copyTo: this.copyTo,
      mailTitle: this.mailTitle,
      ...this.model,
    };
  }

  setLoadingState(value = false) {
    this.isLoading = value;
    if (value) {
      this.$loading.removeAttribute("hidden");
      this.$loading.classList.add(this.activeClass);
    } else {
      this.$loading.setAttribute("hidden", "true");
      this.$loading.classList.remove(this.activeClass);
    }
  }

  formatPayload() {
    const formData = new FormData(this.$form);
    Object.keys(this.payload).forEach((key) => {
      if (this.payload[key] instanceof Date) {
        formData.append(key, this.payload[key].toLocaleString("es"));
      } else if (typeof this.payload[key] === "object") {
        formData.append(key, JSON.stringify(this.payload[key]));
      } else if (this.payload[key]) {
        formData.append(key, this.payload[key]);
      }
    });
    return formData;
  }

  handleResponse({ result = {}, mailResult = {} } = {}) {
    if (result) {
      this.isSuccess();
    } else if (mailResult.status) {
      // eslint-disable-next-line no-console
      console.warn("No se guardó en el drive, pero se envió al correo");
      this.isSuccess();
    } else {
      // eslint-disable-next-line no-console
      console.error("Algo malo sucede");
      this.isError();
    }
    this.setLoadingState(false);
    setTimeout(() => {
      this.preventSending = false;
    }, 5000);
  }

  isSuccess() {
    this.hasSucceded = true;
    this.$form.classList.add("has-success");
    if (this.$success) {
      this.$success.classList.add(this.activeClass);
      this.$success.removeAttribute("hidden");
    }
    if (this.$error) {
      this.$error.classList.remove(this.activeClass);
      this.$error.setAttribute("hidden", "true");
    }
    if (this.thankyou) {
      document.location.href = this.thankyou;
    }
  }

  isError() {
    this.hasSucceded = false;
    this.hasFailed = true;
    this.$form.classList.add("has-error");
    if (this.$error) {
      this.$error.removeAttribute("hidden");
      this.$error.classList.add(this.activeClass);
    }
    if (this.$success) {
      this.$success.setAttribute("hidden", "true");
      this.$success.classList.remove(this.activeClass);
    }
  }

  /**
   * Envia el formulario
   *
   * @param {Event} event El evento del formulario
   *
   * @returns {boolean}
   */
  send(event) {
    event.preventDefault();
    if (this.preventSending) {
      return false;
    }
    this.setLoadingState(true);
    const vm = this;
    const formData = this.formatPayload();
    return fetch(this.endpoint, {
      method: "POST",
      mode: "cors",
      body: formData,
    })
      .then((response) => {
        /* eslint-disable no-console */
        console.log({ response });
        return response.json();
      })
      .then((data) => {
        console.log({ data });
        vm.handleResponse(data);
      })
      .catch((error) => {
        console.error(error);
        return this.isError();
        /* eslint-enable no-console */
      });
  }
}
